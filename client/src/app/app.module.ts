// Modules natifs
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Modules
import { LayoutModule } from '@app/shared/modules/layout/layout.module';
import { AppRoutingModule } from '@app/app.routing.module';

// Components
import { AppComponent } from '@app/app.component';
import { HomepageComponent } from '@app/homepage/homepage.component';
import { TopbarComponent } from '@app/shared/components/topbar/topbar.component';
import { ProfileComponent } from './profile/profile.component';

// Services
import { AuthService } from '@app/shared/services/auth.service';
import { AuthGuard } from '@app/shared/guards/auth.guard';
import { UserService } from '@app/shared/services/user.service';

// Interceptors
import {AuthInterceptor} from '@app/shared/interceptors/auth.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    TopbarComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    AppRoutingModule
  ],
  providers: [
    AuthService,
    AuthGuard,
    UserService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
