import { Component, OnInit } from '@angular/core';

import { UserModel } from '@app/shared/models/user.model';
import { UserService } from '@app/shared/services/user.service';

import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public currentUser: Observable<UserModel>;

  constructor(private userService: UserService) { }

  // Appel userService à l'initialisation du composant
  ngOnInit() {
    this.currentUser = this.userService.getCurrentUser();
  }
}
