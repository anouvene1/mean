import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '@app/shared/services/auth.service';
import { JwtTokenModel } from '@app/shared/models/jwt-token.model';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit, OnDestroy {
  public jwtToken: JwtTokenModel;
  private subscription: Subscription;

  constructor(private authService: AuthService, private router: Router) { }

  /**
   * Récupérer le token via service auth et initialiser la souscription
   */
  ngOnInit() {
    this.subscription = this.authService.jwtToken.subscribe( (token: JwtTokenModel) => {
      this.jwtToken = token;
    });
  }

  /**
   * Appel service auth de déconnexion
   */
  public logout() {
    this.authService.logout();
    // redirection page accueil
    this.router.navigate(['/']);
  }

  /**
   * Se désouscrire du servive auth
   * quand on quitte l'appli pour eviter les fuites mémoire
   */
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
