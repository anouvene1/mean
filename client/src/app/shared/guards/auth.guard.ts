import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';

// Services
import {AuthService} from '@app/shared/services/auth.service';

// RxJS
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators/map';

// Models
import {JwtTokenModel} from '@app/shared/models/jwt-token.model';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  /**
   * Injection des services
   * @param authService Service d'authetification
   * @param router API  Router
   */
  constructor(private authService: AuthService, private router: Router) {}

  /**
   * Protection des routes pour les utilisateurs connectés
   * @param next RouteSnapshot
   * @param state Router
   */
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    return this.authService.jwtToken.pipe(
      map( (token: JwtTokenModel) => {
        if (token.isAuthenticated) {
          return true;
        } else {
          this.router.navigate(['/login/signin']);
          return false;
        }
      })
    );
  }
}
