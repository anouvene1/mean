import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// Modeles
import { UserModel } from '@app/shared/models/user.model';
import { JwtTokenModel } from '@app/shared/models/jwt-token.model';

// RxJS
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators/tap';
import { timer } from 'rxjs/observable/timer';
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Subscription } from 'rxjs/Subscription';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // Token
  public jwtToken: BehaviorSubject<JwtTokenModel> = new BehaviorSubject({
    isAuthenticated: null,
    token: null
  });

  // Subscription to timer Observable
  public subscription: Subscription;

  /**
   * HttpClient
   * @param http HttpClient service
   */
  constructor(private http: HttpClient) {
    this.initToken(); // Appel init token
    this.subscription = this.initTimer(); // Initialisation du timer à l'instanciation du service
  }

  /**
   * Inscription
   * Poster signupForm
   * @param user UserModel
   */
  public signup(user: UserModel): Observable<UserModel> {
    return this.http.post<UserModel>('/api/auth/signup', user);
  }

  /**
   * Connexion
   * Récupérer le token et le conserver localement pendant toute la durée de la connexion
   * Poster signinForm
   * @param credentials Identifiants utilisateur
   */
  public signin(credentials: {email: string, password: string}): Observable<string> {
    return this.http.post<string>('/api/auth/signin', credentials).pipe(
      tap((token: string) => {
        this.jwtToken.next({
          isAuthenticated: true,
          token: token
        });
        localStorage.setItem('jwt', token); // stoker localement le token pour une durée limitée
      })
    );
  }

  /**
   * Déconnexion
   * Mettre à jour le jwtToken Subject
   * Vider le localStorage
   */
  public logout(): void {
    this.jwtToken.next({
      isAuthenticated: false,
      token: null
    });

    localStorage.removeItem('jwt');
  }

  /**
   * Initialiser le token
   * Permettre de déterminer si un utilisateur est connecté selon l'existence ou non du token
   */
  private initToken(): void {
    const token = localStorage.getItem('jwt'); // récupérer le token conservé en local

    if (token) {
      this.jwtToken.next({
        isAuthenticated: true,
        token: token
      });
    } else {
      this.jwtToken.next({
        isAuthenticated: false,
        token: null
      });
    }
  }

  /**
   * Initialiser un timer
   * Si un token existe en local, alors demander un nouveau au serveur back-end
   * Mettre à jour token BehaviorSubject à la derniere valueur retourné par le serveur
   * Puis stoker le nouveau token dans localStorage
   *
   * Si timer renvoie null (pas de token à renouveler), alors mettre jwtToken aux valeurs par defaut
   * Et vider localStorage
   *
   * Penser aussi à désinscrire le timer si pas de token à renouveler
   */
  private initTimer() {
    return timer(2000, 100000).pipe(
      switchMap(() => {
        if (localStorage.getItem('jwt')) {
          console.log('Try refresh the token');

          return this.http.get<string>('/api/auth/refresh-token').pipe(
            tap((token: string) => {
              this.jwtToken.next({
                isAuthenticated: true,
                token: token
              });
              localStorage.setItem('jwt', token);
            })
          );
        } else {
          console.log('No token to refresh !');

          this.subscription.unsubscribe(); // Désinscrire le timer
          return of(null);
        }
      })
    ).subscribe(
      () => {},
      err => {
        this.jwtToken.next({
          isAuthenticated: false,
          token: null
        });
        localStorage.removeItem('jwt');
        this.subscription.unsubscribe(); // Désinscrire le timer
      }
    );
  }
}
