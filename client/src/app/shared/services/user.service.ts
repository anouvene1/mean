import { Injectable } from '@angular/core';
import { UserModel } from '@app/shared/models/user.model';
import { HttpClient } from '@angular/common/http';

// RxJS
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { tap } from 'rxjs/operators/tap';
import { switchMap } from 'rxjs/operators/switchMap';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  // Utilisateur de type BehaviorSubject permettant de partager la même source d'infos
  // à tous les composants qui l'utilise
  public currentUser: BehaviorSubject<UserModel> = new BehaviorSubject(null);

  /**
   * Injection du service HTTP
   * @param http Objet http request
   */
  constructor(private http: HttpClient) { }

  /**
   * Get current user from back-end Http Server
   */
  public getCurrentUser(): Observable<UserModel> {
    if (this.currentUser.value) {
      return this.currentUser;
    } else {
      return this.http.get<UserModel>('/api/user/current').pipe(
        tap( (user: UserModel) => {
          this.currentUser.next(user);
        }),
        switchMap( () => {
          return this.currentUser;
        })
      );
    }
  }
}
