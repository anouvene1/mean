// Modules natifs
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { HomepageComponent } from '@app/homepage/homepage.component';
import { ProfileComponent } from '@app/profile/profile.component';
import { AuthGuard } from '@app/shared/guards/auth.guard';

// Les routes pour les components
const ROUTES: Routes = [
  { path: '', component: HomepageComponent },
  { path: 'login', loadChildren: '@app/login/login.module#LoginModule'},
  { path: 'profile', canActivate: [AuthGuard], component: ProfileComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
