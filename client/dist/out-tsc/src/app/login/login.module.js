"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
// Modules natifs
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
// Modules
var layout_module_1 = require("@app/shared/modules/layout/layout.module");
var login_routing_module_1 = require("@app/login/login-routing.module");
// Components
// import { SigninComponent } from '@app/login/signin/signin.component';
// import { SignupComponent } from '@app/login/signup/signup.component';
var LoginModule = /** @class */ (function () {
    function LoginModule() {
    }
    LoginModule = __decorate([
        core_1.NgModule({
            declarations: [
                login_routing_module_1.LoginRoutingModule.components
                // SigninComponent,
                // SignupComponent
            ],
            imports: [
                common_1.CommonModule,
                layout_module_1.LayoutModule,
                forms_1.ReactiveFormsModule,
                login_routing_module_1.LoginRoutingModule
            ]
        })
    ], LoginModule);
    return LoginModule;
}());
exports.LoginModule = LoginModule;
//# sourceMappingURL=login.module.js.map