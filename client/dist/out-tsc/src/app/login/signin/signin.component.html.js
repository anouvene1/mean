fxLayout;
"column";
var default_1 = /** @class */ (function () {
    function default_1() {
    }
    return default_1;
}());
"py-5";
fxLayoutAlign = "center center" >
    -card >
    -card - title > Connexion < /mat-card-title>
    < mat - card - content >
;
"signinForm";
fxLayout = "column";
fxLayoutGap = "15px";
fxLayoutAlign = "center center" >
    -form - field >
    matInput;
type = "text";
formControlName = "email";
placeholder = "email" >
    /mat-form-field>
    < mat - form - field >
    matInput;
type = "password";
formControlName = "password";
placeholder = "mot de passe" >
    /mat-form-field>
    < div;
var default_2 = /** @class */ (function () {
    function default_2() {
    }
    return default_2;
}());
"text-danger" * ngIf;
"error" > {};
{
    error;
}
/div>
    < button;
mat - raised - button;
color = "primary"(click) = "trySignin()" > Connexion < /button>
    < /form>
    < /mat-card-content>
    < /mat-card>
    < /div>;
//# sourceMappingURL=signin.component.html.js.map