"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
// Modules natifs
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
// Components
var homepage_component_1 = require("@app/homepage/homepage.component");
var profile_component_1 = require("@app/profile/profile.component");
var auth_guard_1 = require("@app/shared/guards/auth.guard");
// Les routes pour les components
var ROUTES = [
    { path: '', component: homepage_component_1.HomepageComponent },
    { path: 'login', loadChildren: '@app/login/login.module#LoginModule' },
    { path: 'profile', canActivate: [auth_guard_1.AuthGuard], component: profile_component_1.ProfileComponent },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(ROUTES)],
            exports: [router_1.RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=app.routing.module.js.map