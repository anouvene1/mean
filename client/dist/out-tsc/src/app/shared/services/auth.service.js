"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var AuthService = /** @class */ (function () {
    /**
     * HttpClient
     * @param http HttpClient service
     */
    function AuthService(http) {
        this.http = http;
        // Token
        this.jwtToken = new rxjs_1.BehaviorSubject({
            isAuthenticated: null,
            token: null
        });
        this.initToken(); // Appel init token
    }
    /**
     * Inscription
     * Poster signupForm
     * @param user UserModel
     */
    AuthService.prototype.signup = function (user) {
        return this.http.post('/api/auth/signup', user);
    };
    /**
     * Connexion
     * Récupérer le token et le conserver localement pendant toute la durée de la connexion
     * Poster signinForm
     * @param credentials Identifiants utilisateur
     */
    AuthService.prototype.signin = function (credentials) {
        var _this = this;
        return this.http.post('/api/auth/signin', credentials).pipe(operators_1.tap(function (token) {
            _this.jwtToken.next({
                isAuthenticated: true,
                token: token
            });
            localStorage.setItem('jwt', token); // stoker localement le token pour une durée limitée
        }));
    };
    /**
     * Déconnexion
     * Mettre à jour le jwtToken Subject
     * Vider le localStorage
     */
    AuthService.prototype.logout = function () {
        this.jwtToken.next({
            isAuthenticated: false,
            token: null
        });
        localStorage.removeItem('jwt');
    };
    /**
     * Initialiser le token
     * Permettre de déterminer si un utilisateur est connecté selon l'existence ou non du token
     */
    AuthService.prototype.initToken = function () {
        var token = localStorage.getItem('jwt'); // récupérer le token conservé en local
        if (token) {
            this.jwtToken.next({
                isAuthenticated: true,
                token: token
            });
        }
        else {
            this.jwtToken.next({
                isAuthenticated: false,
                token: null
            });
        }
    };
    AuthService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], AuthService);
    return AuthService;
}());
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map