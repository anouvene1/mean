"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
// RxJS
var BehaviorSubject_1 = require("rxjs/BehaviorSubject");
var tap_1 = require("rxjs/operators/tap");
var switchMap_1 = require("rxjs/operators/switchMap");
var UserService = /** @class */ (function () {
    /**
     * Injection du service HTTP
     * @param http Objet http request
     */
    function UserService(http) {
        this.http = http;
        // Utilisateur de type BehaviorSubject permettant de partager la même source d'infos
        // à tous les composants qui l'utilise
        this.currentUser = new BehaviorSubject_1.BehaviorSubject(null);
    }
    /**
     * Get current user from back-end Http Server
     */
    UserService.prototype.getCurrentUser = function () {
        var _this = this;
        if (this.currentUser.value) {
            return this.currentUser;
        }
        else {
            return this.http.get('/api/user/current').pipe(tap_1.tap(function (user) {
                _this.currentUser.next(user);
            }), switchMap_1.switchMap(function () {
                return _this.currentUser;
            }));
        }
    };
    UserService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map