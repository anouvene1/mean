"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var auth_service_1 = require("@app/shared/services/auth.service");
var router_1 = require("@angular/router");
var TopbarComponent = /** @class */ (function () {
    function TopbarComponent(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    /**
     * Récupérer le token via service auth et initialiser la souscription
     */
    TopbarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription = this.authService.jwtToken.subscribe(function (token) {
            _this.jwtToken = token;
        });
    };
    /**
     * Appel service auth de déconnexion
     */
    TopbarComponent.prototype.logout = function () {
        this.authService.logout();
        // redirection page accueil
        this.router.navigate(['/']);
    };
    /**
     * Se désouscrire du servive auth
     * quand on quitte l'appli pour eviter les fuites mémoire
     */
    TopbarComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    TopbarComponent = __decorate([
        core_1.Component({
            selector: 'app-topbar',
            templateUrl: './topbar.component.html',
            styleUrls: ['./topbar.component.css']
        }),
        __metadata("design:paramtypes", [auth_service_1.AuthService, router_1.Router])
    ], TopbarComponent);
    return TopbarComponent;
}());
exports.TopbarComponent = TopbarComponent;
//# sourceMappingURL=topbar.component.js.map