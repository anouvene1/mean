-toolbar;
color = "primary";
var default_1 = /** @class */ (function () {
    function default_1() {
    }
    return default_1;
}());
"outline" >
    routerLink;
"/";
var default_2 = /** @class */ (function () {
    function default_2() {
    }
    return default_2;
}());
"img-fluid";
src = "/assets/anouv.png";
alt = "Antoine Nouvene" >
    fxFlex;
"auto" > /span>
    < span;
fxLayout = "row";
fxLayoutAlign = "center center";
fxLayoutGap = "15px" * ngIf;
"!jwtToken.isAuthenticated" >
    -icon;
matTooltip = "Connexion";
var default_3 = /** @class */ (function () {
    function default_3() {
    }
    return default_3;
}());
"btn-link outline";
routerLink = "/login/signin" > power_settings_new < /mat-icon>
    < mat - icon;
matTooltip = "Inscription";
var default_4 = /** @class */ (function () {
    function default_4() {
    }
    return default_4;
}());
"btn-link outline";
routerLink = "/login/signup" > person_add < /mat-icon>
    < /span>
    < span;
fxLayout = "row";
fxLayoutAlign = "center center";
fxLayoutGap = "15px" * ngIf;
"jwtToken.isAuthenticated" >
    -icon;
matTooltip = "Profil";
var default_5 = /** @class */ (function () {
    function default_5() {
    }
    return default_5;
}());
"btn-link outline";
routerLink = "/profile" > account_box < /mat-icon>
    < mat - icon;
matTooltip = "Déconnexion";
var default_6 = /** @class */ (function () {
    function default_6() {
    }
    return default_6;
}());
"btn-link outline"(click) = "logout()" > exit_to_app < /mat-icon>
    < /span>
    < /mat-toolbar>;
//# sourceMappingURL=topbar.component.html.js.map