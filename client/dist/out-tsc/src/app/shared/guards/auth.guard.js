"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
// Services
var auth_service_1 = require("@app/shared/services/auth.service");
var map_1 = require("rxjs/operators/map");
var AuthGuard = /** @class */ (function () {
    /**
     * Injection des services
     * @param authService Service d'authetification
     * @param router API  Router
     */
    function AuthGuard(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    /**
     * Protection des routes pour les utilisateurs connectés
     * @param next RouteSnapshot
     * @param state Router
     */
    AuthGuard.prototype.canActivate = function (next, state) {
        var _this = this;
        return this.authService.jwtToken.pipe(map_1.map(function (token) {
            if (token.isAuthenticated) {
                return true;
            }
            else {
                _this.router.navigate(['/login/signin']);
                return false;
            }
        }));
    };
    AuthGuard = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [auth_service_1.AuthService, router_1.Router])
    ], AuthGuard);
    return AuthGuard;
}());
exports.AuthGuard = AuthGuard;
//# sourceMappingURL=auth.guard.js.map