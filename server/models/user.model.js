const mongoose = require('mongoose'); // ORM mongoose
const Schema = mongoose.Schema; // Schema (Interface) de modelisation d'un objet

// Modelisation des objets selon le Schema suivant:
const userSchema = Schema({
  username: String,
  email: String,
  password: String
});

// Modèle UserModel
const UserModel = mongoose.model('User', userSchema);

// Exporter le modele
module.exports = UserModel;
