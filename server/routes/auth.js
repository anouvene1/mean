'use strict';

// Importer le modèle UserModel
const UserModel = require('../models/user.model');

// Routeur Express
const router = require('express').Router();

// Cryptage password
const bcrypt = require('bcrypt'); // npm i bcrypt --save

// Librairie pour créer un token
const jwt = require('jsonwebtoken');

// Librairie native de Node pour lire et ecrire un fichier
const fs = require('fs');

// Clé privée
const RSA_KEY_PRIVATE = fs.readFileSync('./rsa/key');

// Clé publique
const RSA_PUBLIC_KEY = fs.readFileSync('./rsa/key.pub');

/**
 * Connexion
 * Verifier que l'utilisateur existe dans la base via son email
 * Véfifier la validité du password et créer un token crypté avec jsonwebtoken
 * Renvoyer le nouveau token à l'utilisateur
 */
router.post('/signin', (req, res) => {
  UserModel.findOne({ 'email': req.body.email }).exec((err, user) => {
    if (user && bcrypt.compareSync(req.body.password, user.password)) {
      const token = jwt.sign({}, RSA_KEY_PRIVATE, {
        algorithm: 'RS256',
        subject: user._id.toString()
      });
      res.status(200).json(token);
    } else {
      res.status(401).json('signin fail !');
    }
  });
});

/**
 * Inscription
 */
router.post('/signup', (req, res) => {
  // console.log('Request body: ', req.body);
  const newUser = new UserModel({
    username: req.body.username,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8))
  });

  newUser.save((err) => {
    if (err) { res.status(500).json('erreur signup !'); }
    res.status(200).json('signup ok !');
  });
});

/**
 * Renouveler le token toutes les 15 secondes
 */
router.get('/refresh-token', (req, res) => {
  const token = req.headers.authorization;
  console.log('Token: ', token);

  if (token) {
    jwt.verify(token, RSA_PUBLIC_KEY, (err, decoded) => {
      if (err) { return res.status(403).json('Wrong token ! ' + err.name + " - " + err.message); }
      const newToken = jwt.sign({}, RSA_KEY_PRIVATE, {
        algorithm: 'RS256',
        expiresIn: '24h', // Pour éviter les vols ou les copies de token
        subject: decoded.sub
      });
      res.status(200).json(newToken);
    });
  } else {
    res.status(403).json('No token to refresh !');
  }
});

module.exports = router;
