'use strict';

// Importer le modèle UserModel
const UserModel = require('../models/user.model');

// Routeur Express
const router = require('express').Router();

const fs = require('fs');
const RSA_PUBLIC_KEY = fs.readFileSync('./rsa/key.pub');
const jwt = require('jsonwebtoken');

function isLoggedIn (req, res, next) {
  // Récupérer le token dans header authorization
  const token = req.headers.authorization;

  if (token) { // si token existe
    // jsonwebtoken verifie l'authenticité du token via la cle phblique
    jwt.verify(token, RSA_PUBLIC_KEY, (err, decoded) => {
      // Si token non valide, retourner l'erreur 401 Unauthorized
      if (err) { return res.status(401).json('token invalid'); }

      // Si token valide, trouver un utilisateur via son id contenu dans
      // la propriété sub
      const sub = decoded.sub;

      UserModel.findOne({ '_id': sub }).exec((err, user) => {
        if (err || !user) { res.status(401).json('error'); }
        req.user = user; // Attacher l'objet user à la requête à retourner
        next(); // Passer la main au prochain middleware
      });
    });
  } else {
    res.status(401).json('pas de token !');
  }
}

// En l'occurrence, le prochain middleware est :
router.get('/current', isLoggedIn, (req, res) => {
  // Retourner l'utilisateur que nous avons attaché sur l'objet request
  res.json(req.user);
});

module.exports = router;
