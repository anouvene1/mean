'use strict';

const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const app = express();
const index = require('./routes/index');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../public')));

// Connexion à la base de données
// https://mlab.com/login
mongoose.connect('mongodb://anouv:1234azerti@ds225442.mlab.com:25442/andb', {
  keepAlive: true,
  reconnectTries: Number.MAX_VALUE
}, function (error) {
  if (!error) {
    console.log('Connexion opened to mongodb!');
  } else {
    console.log(error);
  }
});

app.use(index);

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/index.html'));
});

module.exports = app;
